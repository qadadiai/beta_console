#pragma once
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"
#include "Options.hpp"


class Actigo90 : public Option {
public:
	PnlVect *PayOff_coefficient;
	Actigo90();
	Actigo90(double T, int nbTimeSteps, int size);
	Actigo90(const Actigo90 &A);
	Actigo90& operator=(const Actigo90 &A);
	~Actigo90();
	double payoff(const PnlMat *path, bool type_path = true);
};