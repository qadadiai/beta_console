#ifndef ASIAN_H
#define	ASIAN_H

#pragma once

#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"
#include "Options.hpp"

class Asian : public Option {
public:
	double strike_;
	PnlVect *PayOff_coefficient;
	Asian();
	Asian(double T, int nbTimeSteps, int size, double strike, PnlVect *payOff_coefficient);
	Asian(const Asian &A);
	Asian& operator=(const Asian &A);
	~Asian();
	double payoff(const PnlMat *path, bool boolean = true);
};

#endif