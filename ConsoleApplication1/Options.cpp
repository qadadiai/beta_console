#pragma once
#include "pch.h"
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"
#include "Options.hpp"
/// \brief Classe Option abstraite


Option::Option() {
	T_ = 0.0;
	nbTimeSteps_ = 0;
	size_ = 0;
}

Option::Option(double T, int nbTimeSteps, int size) {
	T_ = T;
	nbTimeSteps_ = nbTimeSteps;
	size_ = size;
}

/*Option::Option(const Option &O) {
  T_ = O.T_ ;
  nbTimeSteps_ =O.nbTimeSteps_;
  size_ = O.size_;
}*/

/*Option::Option& operator=(const Option &O) {
  T_ = O.T_ ;
  nbTimeSteps_ =O.nbTimeSteps_;
  size_ = O.size_;
  return *this ;
}*/


Option::~Option() {
}