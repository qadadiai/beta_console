#include "Portefeuille.hpp"
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"
#include "BlackScholesModel.hpp"
#include "Computations.hpp"
#include "pch.h"
#include <cmath>


Portefeuille::Portefeuille() {
	Deltas = pnl_vect_new();
	Assets = pnl_vect_new();
	zero_coupon = 0;
	Interest = 0;
}



Portefeuille::Portefeuille(PnlVect *Deltas, double zero_coupon, PnlVect *Assets,double Interest, double maturity, double H) {
	this->Deltas = pnl_vect_copy(Deltas);
	this->zero_coupon = zero_coupon;
	this->Assets = pnl_vect_copy(Assets);
	this->Interest = Interest;
	this->maturity = maturity;
	this->H = H;

}


Portefeuille::Portefeuille(const Portefeuille &Pf) {
	Deltas = Pf.Deltas;
	zero_coupon = Pf.zero_coupon;
	Assets = Pf.Assets;
	Interest = Pf.Interest;
}
Portefeuille &Portefeuille::operator=(const Portefeuille &Pf) {
	Deltas = Pf.Deltas;
	zero_coupon = Pf.zero_coupon;
	Assets = Pf.Assets;
	Interest = Pf.Interest;
	return *this;
}
Portefeuille::~Portefeuille() {
	pnl_vect_free(&Deltas);
	pnl_vect_free(&Assets);
}
double Portefeuille::Pf_Value() {
	double value = zero_coupon;
	PnlVect *V = pnl_vect_copy(this->Assets);
	//pnl_vect_mult_scalar(V, 1 / 3);//assets*shares = composition du portefeuille
	value += pnl_vect_scalar_prod(Deltas, V);
	pnl_vect_free(&V);
	return value;
}
void Portefeuille::Update_Pf(PnlVect *Deltas,PnlVect *Assets) {
	zero_coupon = zero_coupon * exp(Interest*(this->maturity/this->H));
	this->Assets = pnl_vect_copy(Assets);
	PnlVect *X = pnl_vect_copy(this->Assets);
	//pnl_vect_mult_scalar(X, 1 / 3);
	PnlVect *V = pnl_vect_copy(this->Deltas);
	pnl_vect_minus_vect(V, Deltas);
	zero_coupon += pnl_vect_scalar_prod(V,X);
	this->Deltas = pnl_vect_copy(Deltas);
	pnl_vect_free(&V);
	pnl_vect_free(&X);
}