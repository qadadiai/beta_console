#pragma once
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"

/// \brief Classe Option abstraite
class Option
{
public:
	double T_; /// maturit�
	int nbTimeSteps_; /// nombre de pas de temps de discr�tisation
	int size_; /// dimension du mod�le, redondant avec BlackScholesModel::size_
	Option();
	Option(double T, int nbTimeSteps, int size);
	/*Option(const Option &O);
	Option& operator=(const Option &O);*/
	~Option();
	/**
	 * Calcule la valeur du payoff sur la trajectoire
	 *
	 * @param[in] path est une matrice de taille (N+1) x d
	 * contenant une trajectoire du mod�le telle que cr��e
	 * par la fonction asset.
	 * @return phi(trajectoire)
	 */
	virtual double payoff(const PnlMat *path, bool boolean = true) = 0;
};