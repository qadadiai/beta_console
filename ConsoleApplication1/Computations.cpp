#include "pch.h"
#include "Computations.hpp"
#include "BlackScholesModel.hpp"
#include "MonteCarlo.hpp"
#include "Actigo90.hpp"
#include <iostream>
#include <time.h>
#include "pnl/pnl_random.h"
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"

using namespace std;


void Computations::calleuro(double &ic, double &prix, int nb_samples, double T,
	double S0, double K, double sigma, double r)
{
	double drift = (r - sigma * sigma / 2.) * T;
	double sqrt_T = sqrt(T);
	double sum = 0;
	double var = 0;
	PnlRng *rng = pnl_rng_create(PNL_RNG_MERSENNE);
	pnl_rng_sseed(rng, time(NULL));
	double payoff;
	for (int i = 0; i < nb_samples; i++)
	{
		payoff = S0 * exp(drift + sigma * sqrt_T * pnl_rng_normal(rng));
		payoff = MAX(payoff - K, 0.);
		sum += payoff;
		var += payoff * payoff;
	}

	prix = exp(-r * T) * sum / nb_samples;
	var = exp(-2.*r*T) * var / nb_samples - prix * prix;
	ic = 1.96 * sqrt(var / nb_samples);
	pnl_rng_free(&rng);
}

void Computations::testPayoffActigo(double &prix)
{
	double x = 0;
	double T = 365 * 8;
	double nbTimeStep = 16;
	int size = 3;
	Actigo90 *actigo = new Actigo90(T, nbTimeStep, size);
	PnlMat *Path = pnl_mat_create(17, 3);
	//remplir Path par les donn�es.
	//pnl_mat_set(Path,i,j,x) ,double pnl_mat_get(Path,i,j)
	pnl_mat_set(Path, 0, 0, 3069.43);
	pnl_mat_set(Path, 0, 1, 1931.72);
	pnl_mat_set(Path, 0, 2, 5241);
	pnl_mat_set(Path, 1, 0, 2983.49);
	pnl_mat_set(Path, 1, 1, 1902.74);
	pnl_mat_set(Path, 1, 2, 5476.85);
	pnl_mat_set(Path, 2, 0, 2983.49);
	pnl_mat_set(Path, 2, 1, 1902.74);
	pnl_mat_set(Path, 2, 2, 5476.85);
	pnl_mat_set(Path, 3, 0, x);
	pnl_mat_set(Path, 3, 1, x);
	pnl_mat_set(Path, 3, 2, x);
	pnl_mat_set(Path, 4, 0, x);
	pnl_mat_set(Path, 4, 1, x);
	pnl_mat_set(Path, 4, 2, x);
	pnl_mat_set(Path, 5, 0, x);
	pnl_mat_set(Path, 5, 1, x);
	pnl_mat_set(Path, 5, 2, x);
	pnl_mat_set(Path, 6, 0, x);
	pnl_mat_set(Path, 6, 1, x);
	pnl_mat_set(Path, 6, 2, x);
	pnl_mat_set(Path, 7, 0, x);
	pnl_mat_set(Path, 7, 1, x);
	pnl_mat_set(Path, 7, 2, x);
	pnl_mat_set(Path, 8, 0, x);
	pnl_mat_set(Path, 8, 1, x);
	pnl_mat_set(Path, 8, 2, x);
	pnl_mat_set(Path, 9, 0, x);
	pnl_mat_set(Path, 9, 1, x);
	pnl_mat_set(Path, 9, 2, x);
	pnl_mat_set(Path, 10, 0, x);
	pnl_mat_set(Path, 10, 1, x);
	pnl_mat_set(Path, 10, 2, x);
	pnl_mat_set(Path, 11, 0, x);
	pnl_mat_set(Path, 11, 1, x);
	pnl_mat_set(Path, 11, 2, x);
	pnl_mat_set(Path, 12, 0, x);
	pnl_mat_set(Path, 12, 1, x);
	pnl_mat_set(Path, 12, 2, x);
	pnl_mat_set(Path, 13, 0, x);
	pnl_mat_set(Path, 13, 1, x);
	pnl_mat_set(Path, 13, 2, x);
	pnl_mat_set(Path, 14, 0, x);
	pnl_mat_set(Path, 14, 1, x);
	pnl_mat_set(Path, 14, 2, x);
	pnl_mat_set(Path, 15, 0, x);
	pnl_mat_set(Path, 15, 1, x);
	pnl_mat_set(Path, 15, 2, x);
	pnl_mat_set(Path, 16, 0, x);
	pnl_mat_set(Path, 16, 1, x);
	pnl_mat_set(Path, 16, 2, x);
	double resultat_attendu = (0.07) / 8;
	double resultat_trouve = actigo->payoff(Path);
	prix = resultat_trouve;
	resultat_attendu = (0.7 / 8);
	//cout << resultat_attendu - resultat_trouve << endl;
}

void Computations::testAsianOption(double &prix, double &ic);

double Computations::prix_en_pourcentage(double &resultat_attendu)
 {
	double x = 0;
	double T = 365 * 8;
	double nbTimeStep = 16;
	int size = 3;
	Actigo90 * actigo = new Actigo90(T, nbTimeStep, size);
	PnlMat * Path = pnl_mat_create(17, 3);
	       //remplir Path par les donn<E9>es.
		       //pnl_mat_set(Path,i,j,x) ,double pnl_mat_get(Path,i,j)
		pnl_mat_set(Path, 0, 0, 0);
	pnl_mat_set(Path, 0, 1, 0);
	pnl_mat_set(Path, 0, 2, 0);
	pnl_mat_set(Path, 1, 0, -2.8);
	pnl_mat_set(Path, 1, 1, -1.5);
	pnl_mat_set(Path, 1, 2, 4.5);
	pnl_mat_set(Path, 2, 0, -5.60);
	pnl_mat_set(Path, 2, 1, 2.40);
	pnl_mat_set(Path, 2, 2, 6.30);
	pnl_mat_set(Path, 3, 0, -10.20);
	pnl_mat_set(Path, 3, 1, 2.10);
	pnl_mat_set(Path, 3, 2, 6.80);
	pnl_mat_set(Path, 4, 0, -9.30);
	pnl_mat_set(Path, 4, 1, -2.50);
	pnl_mat_set(Path, 4, 2, 14.70);
	pnl_mat_set(Path, 5, 0, -18.30);
	pnl_mat_set(Path, 5, 1, 3.20);
	pnl_mat_set(Path, 5, 2, 10.80);
	pnl_mat_set(Path, 6, 0, -14.50);
	pnl_mat_set(Path, 6, 1, 1.30);
	pnl_mat_set(Path, 6, 2, 12.70);
	pnl_mat_set(Path, 7, 0, -12.75);
	pnl_mat_set(Path, 7, 1, 9.20);
	pnl_mat_set(Path, 7, 2, -5);
	pnl_mat_set(Path, 8, 0, -18.30);
	pnl_mat_set(Path, 8, 1, 7.20);
	pnl_mat_set(Path, 8, 2, -8.30);
	pnl_mat_set(Path, 9, 0, -23.50);
	pnl_mat_set(Path, 9, 1, 3.20);
	pnl_mat_set(Path, 9, 2, -14.70);
	pnl_mat_set(Path, 10, 0, -22.70);
	pnl_mat_set(Path, 10, 1, -1.10);
	pnl_mat_set(Path, 10, 2, -17.30);
	pnl_mat_set(Path, 11, 0, -21.80);
	pnl_mat_set(Path, 11, 1, -6.70);
	pnl_mat_set(Path, 11, 2, -12.20);
	pnl_mat_set(Path, 12, 0, -21);
	pnl_mat_set(Path, 12, 1, -14.10);
	pnl_mat_set(Path, 12, 2, -19.00);
	pnl_mat_set(Path, 13, 0, -17.10);
	pnl_mat_set(Path, 13, 1, -11.50);
	pnl_mat_set(Path, 13, 2, -26.70);
	pnl_mat_set(Path, 14, 0, -19.50);
	pnl_mat_set(Path, 14, 1, -16.45);
	pnl_mat_set(Path, 14, 2, -15.70);
	pnl_mat_set(Path, 15, 0, -4.20);
	pnl_mat_set(Path, 15, 1, -3.10);
	pnl_mat_set(Path, 15, 2, -1.40);
	pnl_mat_set(Path, 16, 0, 12.40);
	pnl_mat_set(Path, 16, 1, 10.20);
	pnl_mat_set(Path, 16, 2, 7.80);
	resultat_attendu = 0.9076;
	double resultat_trouve = actigo->payoff(Path, false);
	
	return resultat_trouve;
	       
		}
// test�
void Computations::testPayoffActigo(double &prix, double &resultat_attendu, bool type_path) {
	if (type_path) {
		prix = Computations::prix_sans_pourcentage(resultat_attendu);
		
	}
	else {
		prix = Computations::prix_en_pourcentage(resultat_attendu);
		
	}
	
}


// fonction pas encore test� : calcul du prix du produit.
void Computations::PrixProduit(double &prix,double &ic) {
		// /!\  on test juste pour rho fixe et la m�me pour les 5 equations. faut passer au cas ou rho et une matrice :)
	       double size = 3;
		   double r = 0.1;
		   PnlVect *volatilities = pnl_vect_create(3);
		   PnlVect *trend = pnl_vect_create(3);
		   PnlVect *spot = pnl_vect_create(3);
		   double rho = 0.25;
		   int H = 16;
		   pnl_vect_set(volatilities, 0,0.5 );
		   pnl_vect_set(volatilities, 1,0.5 );
		   pnl_vect_set(volatilities, 2,0.5 );
		   pnl_vect_set(spot, 0, 3069.43 );
		   pnl_vect_set(spot, 1, 1931.72 );
		   pnl_vect_set(spot, 2, 5241 );
		   pnl_vect_set(trend, 0, 0.1);
		   pnl_vect_set(trend, 1, 0.1);
		   pnl_vect_set(trend, 2, 0.1);
	       BlackScholesModel bc = BlackScholesModel(size, r, rho, volatilities, spot, trend);
		   double T =  8;
		   //double nbTimeStep = 16;
		   //int size = 3;
		   PnlRng *rng = pnl_rng_create(PNL_RNG_MERSENNE);
		   pnl_rng_sseed(rng, time(NULL));
		   Actigo90  actigo = Actigo90(T, H, size);
		   MonteCarlo mc = MonteCarlo(&bc,&actigo,1E6,rng,0.001);
		   mc.price(prix,ic);
}
void Computations::PrixProduitAndDeltas(double &prix, double &ic , PnlVect *delta, PnlVect *icDelta, double t, PnlMat *past ) {
	// /!\  on test juste pour rho fixe et la m�me pour les 5 equations. faut passer au cas ou rho et une matrice :)
	double size = 3;
	double r = 0.1;
	PnlVect *volatilities = pnl_vect_create(3);
	PnlVect *trend = pnl_vect_create(3);
	PnlVect *spot = pnl_vect_create(3);
	double rho = 0.25;// correlation mvt brownien , cas � traiter : matrice de corr�lation 
	int H = 16;//nombres de dates de constatations
	pnl_vect_set(volatilities, 0, 0.5);
	pnl_vect_set(volatilities, 1, 0.5);
	pnl_vect_set(volatilities, 2, 0.5);
	pnl_vect_set(spot, 0, 3069.43);
	pnl_vect_set(spot, 1, 1931.72);
	pnl_vect_set(spot, 2, 5241);
	pnl_vect_set(trend, 0, 0.1);
	pnl_vect_set(trend, 1, 0.1);
	pnl_vect_set(trend, 2, 0.1);
	BlackScholesModel bc = BlackScholesModel(size, r, rho, volatilities, spot, trend);
	double T = 8;
	//double nbTimeStep = 16;
	//int size = 3;
	PnlRng *rng = pnl_rng_create(PNL_RNG_MERSENNE);
	pnl_rng_sseed(rng, time(NULL));
	Actigo90  actigo = Actigo90(T, H, size);
	MonteCarlo mc = MonteCarlo(&bc, &actigo, 1E6, rng, 0.001);
	mc.price(past,t,prix, ic);
	mc.delta(past, t, delta, icDelta);
}


void Computations::listPrices(PnlVect *listPrix) {
	double size = 3;
	double nbTimeSteps = 251 * 8;
	PnlMat *path = pnl_mat_create(nbTimeSteps, size);

	
}
void Computations::profitAndLost(double &PL) {
	double size = 3;
	double r = 0.1;
	PnlVect *volatilities = pnl_vect_create(3);
	PnlVect *trend = pnl_vect_create(3);//vecteur de taux d'int�r�t(euro,USA,autralie).
	PnlVect *spot = pnl_vect_create(3);
	double rho = 0.25;// correlation mvt brownien , cas � traiter : matrice de corr�lation 
	int H = 32;// nombre de points de simulations
	int N = 16;// nombres de dates de constatations
	pnl_vect_set(volatilities, 0, 0.5);
	pnl_vect_set(volatilities, 1, 0.5);
	pnl_vect_set(volatilities, 2, 0.5);
	pnl_vect_set(spot, 0, 3069.43);
	pnl_vect_set(spot, 1, 1931.72);
	pnl_vect_set(spot, 2, 5241);
	pnl_vect_set(trend, 0, 0.1);
	pnl_vect_set(trend, 1, 0.1);
	pnl_vect_set(trend, 2, 0.1);
	BlackScholesModel bc = BlackScholesModel(size, r, rho, volatilities, spot, trend);
	double T = 8;
	//double nbTimeStep = 16;
	//int size = 3;
	PnlMat *marche = pnl_mat_create(H+1, size);
	PnlRng *rng = pnl_rng_create(PNL_RNG_MERSENNE);
	PnlRng *rng2 = pnl_rng_create(PNL_RNG_MERSENNE);
	pnl_rng_sseed(rng, time(NULL));
	bc.simul_market(marche, T, H, rng);
	pnl_rng_sseed(rng2, time(NULL));
	Actigo90  actigo = Actigo90(T, N, size);
	MonteCarlo mc = MonteCarlo(&bc, &actigo, 50000, rng2, 0.001);
	PnlVect *listPrix = pnl_vect_create(H+1);
	mc.PL(marche, listPrix, PL);
}
// Couverture et Calcul des Deltas( depends des params rho et volatilit�s qu'on prendra pour simuler)
// � faire le weekend


double Computations::prix_sans_pourcentage(double &resultat_attendu) { return 0; };


void Computations::test_complexite_Deltas() {
	
	double size = 3;
	double r = 0.1;
	PnlVect *volatilities = pnl_vect_create(3);
	PnlVect *trend = pnl_vect_create(3);//vecteur de taux d'int�r�t(euro,USA,autralie).
	PnlVect *spot = pnl_vect_create(3);
	double rho = 0.25;// correlation mvt brownien , cas � traiter : matrice de corr�lation 
	int H = 250*8 ;// nombre de points de simulations
	int N = 16;// nombres de dates de constatations
	pnl_vect_set(volatilities, 0, 0.5);
	pnl_vect_set(volatilities, 1, 0.5);
	pnl_vect_set(volatilities, 2, 0.5);
	pnl_vect_set(spot, 0, 3069.43);
	pnl_vect_set(spot, 1, 1931.72);
	pnl_vect_set(spot, 2, 5241);
	pnl_vect_set(trend, 0, 0.1);
	pnl_vect_set(trend, 1, 0.1);
	pnl_vect_set(trend, 2, 0.1);
	BlackScholesModel bc = BlackScholesModel(size, r, rho, volatilities, spot, trend);
	
	double T = 8;
	//double nbTimeStep = 16;
	//int size = 3;
	PnlRng *rng = pnl_rng_create(PNL_RNG_MERSENNE);
	pnl_rng_sseed(rng, time(NULL));
	Actigo90  actigo = Actigo90(T, N, size);
	MonteCarlo mc = MonteCarlo(&bc, &actigo, 1E3, rng, 0.001);
	PnlVect *Deltas = pnl_vect_create(mc.mod_->size_);
	PnlVect *icdelta = pnl_vect_create(mc.mod_->size_);
	PnlRng *rng2 = pnl_rng_create(PNL_RNG_MERSENNE);
	pnl_rng_sseed(rng2, time(NULL));
	PnlMat *marche = pnl_mat_create(H, mc.mod_->size_);
	bc.simul_market(marche, T, H, rng2);
	PnlMat *past  = pnl_mat_create_from_scalar(mc.opt_->nbTimeSteps_, mc.opt_->size_, 0);
	for (int i = 1; i <= H - 1; i++) {
		mc.passe_adapte(i, marche, past);
		double t = i * (mc.opt_->T_) / H;
		cout << " Delta de  " << i << " de temps associ�  " << t << " est : " << endl;
		mc.delta(past, t, Deltas, icdelta);
		pnl_vect_print(Deltas);
	}

}
void Computations::test_Asset() {
	double size = 3;
	double r = 0.1;
	double rho = 0.25;// correlation mvt brownien , cas � traiter : matrice de corr�lation 
	int H = 250 * 8;// nombre de points de simulations
	int N = 16;// nombres de dates de constatations
	double T = 8;


	PnlVect *volatilities = pnl_vect_create(3);
	PnlVect *trend = pnl_vect_create(3);//vecteur de taux d'int�r�t(euro,USA,autralie).
	PnlVect *spot = pnl_vect_create(3);
	pnl_vect_set(volatilities, 0, 0.5);
	pnl_vect_set(volatilities, 1, 0.5);
	pnl_vect_set(volatilities, 2, 0.5);
	pnl_vect_set(spot, 0, 3069.43);
	pnl_vect_set(spot, 1, 1931.72);
	pnl_vect_set(spot, 2, 5241);
	pnl_vect_set(trend, 0, 0.1);
	pnl_vect_set(trend, 1, 0.1);
	pnl_vect_set(trend, 2, 0.1);


	PnlRng *rng = pnl_rng_create(PNL_RNG_MERSENNE);
	pnl_rng_sseed(rng, time(NULL));

	BlackScholesModel bc = BlackScholesModel(size, r, rho, volatilities, spot, trend);
	Actigo90  actigo = Actigo90(T, N, size);
	MonteCarlo mc = MonteCarlo(&bc, &actigo, 1E3, rng, 0.001); 


	PnlMat *past = pnl_mat_create_from_scalar(mc.opt_->nbTimeSteps_, mc.opt_->size_, 0);
	pnl_mat_set_row(past, spot, 0);
	pnl_mat_set_row(past, spot, 1);


	PnlMat *path = pnl_mat_create_from_scalar(mc.opt_->nbTimeSteps_, mc.opt_->size_, 0);
	mc.mod_->asset(path, 0.51, mc.opt_->T_, mc.opt_->nbTimeSteps_, mc.rng_, past);
	pnl_mat_print(path);
}

/*void Computations::Test_Couverture() {
	double size = 3;
	double r = 0.1;
	double rho = 0.25;// correlation mvt brownien , cas � traiter : matrice de corr�lation 
	int H = 250 * 8;// nombre de points de simulations
	int N = 16;// nombres de dates de constatations
	double T = 8;



	PnlVect *volatilities = pnl_vect_create(3);
	PnlVect *trend = pnl_vect_create(3);//vecteur de taux d'int�r�t(euro,USA,autralie).
	PnlVect *spot = pnl_vect_create(3);
	pnl_vect_set(volatilities, 0, 0.5);
	pnl_vect_set(volatilities, 1, 0.5);
	pnl_vect_set(volatilities, 2, 0.5);
	pnl_vect_set(spot, 0, 3069.43);
	pnl_vect_set(spot, 1, 1931.72);
	pnl_vect_set(spot, 2, 5241);
	pnl_vect_set(trend, 0, 0.1);
	pnl_vect_set(trend, 1, 0.1);
	pnl_vect_set(trend, 2, 0.1);


	PnlRng *rng = pnl_rng_create(PNL_RNG_MERSENNE);
	pnl_rng_sseed(rng, time(NULL));

	BlackScholesModel bc = BlackScholesModel(size, r, rho, volatilities, spot, trend);
	Actigo90  actigo = Actigo90(T, N, size);
	MonteCarlo mc = MonteCarlo(&bc, &actigo, 1E3, rng, 0.001);
	PnlMat *past = pnl_mat_create_from_double(mc.opt_->nbTimeSteps_ + 1, mc.opt_->size_, 0.);
	PnlVect *Assets = pnl_vect_create_from_double(mc.mod_->size_, 0.);


	pnl_mat_get_row(Assets, marche, 0);
	pnl_mat_set_row(past, Assets, 0);


	double p_0 = 0;
	double ic = 0;
	this->price(p_0, ic);
	PnlVect *icdelta = pnl_vect_create_from_double(mod_->size_, 0.);
	PnlVect *delta = pnl_vect_create_from_double(mod_->size_, 0.);
	this->delta(past, 0.0, delta, icdelta);
	pnl_mat_get_row(Assets, marche, 0);
	double zero_coupon = p_0 - pnl_vect_scalar_prod(delta, Assets);
	//PL = zero_coupon;
	Portefeuille *Pf_couverture = new Portefeuille(delta, zero_coupon, Assets, mod_->r_, opt_->T_, H);
}*/

