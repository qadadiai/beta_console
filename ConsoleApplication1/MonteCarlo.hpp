#pragma once
#include "Options.hpp"
#include "Asian.hpp"
#include "BlackScholesModel.hpp"
#include "pnl/pnl_random.h"
#include <iostream>
#include <string>

class MonteCarlo
{
public:
	BlackScholesModel *mod_; /*! pointeur vers le mod�le */
	Option *opt_; /*! pointeur sur l'option */
	PnlRng *rng_; /*! pointeur sur le g�n�rateur */
	double fdStep_; /*! pas de diff�rence finie */
	size_t nbSamples_; /*! nombre de tirages Monte Carlo */

	MonteCarlo();
	/*MonteCarlo& operator = (const MonteCarlo &BSM);
	MonteCarlo(const MonteCarlo &MC);*/
	~MonteCarlo();
	MonteCarlo(BlackScholesModel *mod, Option *opt, int nbSamples, PnlRng *rng, double fdStep);

	/**
	 * Calcule le prix de l'option � la date 0
	 *
	 * @param[out] prix valeur de l'estimateur Monte Carlo
	 * @param[out] ic largeur de l'intervalle de confiance
	 */
	void price(double &prix, double &ic);

	/**
	 * Calcule le prix de l'option � la date t
	 *
	 * @param[in]  past contient la trajectoire du sous-jacent
	 * jusqu'� l'instant t
	 * @param[in] t date � laquelle le calcul est fait
	 * @param[out] prix contient le prix
	 * @param[out] ic contient la largeur de l'intervalle
	 * de confiance sur le calcul du prix
	 */
	void price(const PnlMat *past, double t, double &prix, double &ic);

	/**
	 * Calcule le delta de l'option � la date t
	 *
	 * @param[in] past contient la trajectoire du sous-jacent
	 * jusqu'� l'instant t
	 * @param[in] t date � laquelle le calcul est fait
	 * @param[out] delta contient le vecteur de delta
	 * de confiance sur le calcul du delta
	 */
	void delta(const PnlMat *past, double t, PnlVect *delta, PnlVect *ic);


	void Profit_and_loss(const PnlMat*past, PnlVect *list_prices , double &PL);

	/*return une list des prix pour chaque jour
	* Utilit� : dessin graphe tracking error
	*
	* @param[in] : donn� de march� simul� par d'un simulateur du marche Simul_Market
	* @param[out] : Vecteur(PnlVect) qui contient 251*8 prix au long de 8 ans.
	*/
	PnlVect *prices_list(const PnlMat *marche,PnlVect *ic);


	void PL(const PnlMat* marche, PnlVect *prices_list, double &PL);


	void passe_adapte(int i,const PnlMat *marche, PnlMat *past);

};
#pragma once
