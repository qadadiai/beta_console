#include "pch.h"
#include "MonteCarlo.hpp"
#include "BlackScholesModel.hpp"
#include "Options.hpp"
#include "Actigo90.hpp"
#include "Portefeuille.hpp"
#include <cmath>
using namespace std;

/* Constructeur par d�fault */
MonteCarlo::MonteCarlo() {
	mod_ = new BlackScholesModel();
	opt_ = new Actigo90();
	rng_ = pnl_rng_create(PNL_RNG_MERSENNE);
	fdStep_ = 0;
	nbSamples_ = 0;
}

/** Constructeur complet du MonteCarlo */
MonteCarlo::MonteCarlo(BlackScholesModel *mod, Option *opt, int nbSamples, PnlRng *rng, double fdStep) {
	mod_ = mod;
	opt_ = opt;	
	rng_ = pnl_rng_copy(rng);
	fdStep_ = fdStep;
	nbSamples_ = nbSamples;
}

/* Destrcuteur de MonteCarlo */
MonteCarlo::~MonteCarlo() {
	opt_->~Option();
	mod_->~BlackScholesModel();
	pnl_rng_free(&rng_);
}

/** Calcul du prix */
void MonteCarlo::price(double &prix, double &ic) {
	PnlMat *path = pnl_mat_create(opt_->nbTimeSteps_ + 1, opt_->size_);
	double results = 0;
	double esp_carre = 0;
	for (int i = 0; i < nbSamples_; i++) {
		mod_->asset(path, opt_->T_, opt_->nbTimeSteps_, rng_);
		double payOff = opt_->payoff(path);
		results += payOff;
		esp_carre += payOff * payOff;
	}
	results = (results / nbSamples_);
	esp_carre = (esp_carre / nbSamples_);
	double esp = results;
	prix = exp(-mod_->r_*(opt_->T_))*results;

	double estim_carre = exp(-2 * mod_->r_*(opt_->T_))*(esp_carre - esp * esp);
	ic = 2 * 1.96*sqrt(estim_carre / nbSamples_);
	pnl_mat_free(&path);
}

/** Calcul du Delta */
void MonteCarlo::delta(const PnlMat *past, double t, PnlVect *delta, PnlVect *ic) {

	//initialisation de la matrice path.
	PnlMat *path = pnl_mat_create(opt_->nbTimeSteps_ + 1, opt_->size_);
	double timestep = opt_->T_ / opt_->nbTimeSteps_;

	//pas de la m�thode.
	double h = fdStep_;

	//initialisation des matrices shift_path pour +h(et shift_path_inverse pour -h)
	PnlMat *shift_path = pnl_mat_create_from_scalar(opt_->nbTimeSteps_ + 1, opt_->size_, 0);
	PnlMat *shift_path_inverse = pnl_mat_create_from_scalar(opt_->nbTimeSteps_ + 1, opt_->size_, 0);

	//Delta calculer pour chaque actif
	double Delta = 0.;
	//trouver l'indice o� est localis� les valeurs des actifs � l'instant
	int compteur = 0;
	while ((pnl_mat_get(past,compteur,0) != 0.) && (compteur < past->m-1) )
	{
		compteur++;
	}
	//vecteur des valeurs des actifs � l'instant t.
	PnlVect *V = pnl_vect_create(mod_->size_);
	pnl_mat_get_row(V, past, compteur-1);

	double param_actualisation = 0.;
	for (int d = 1; d < path->n + 1; d++) {
		double var = 0.;
		Delta = 0.;
		double diff = 0.0;
		param_actualisation = (exp(-mod_->r_*(opt_->T_ - t))) / (nbSamples_ * 2 * h);
		for (int i__ = 0; i__ < nbSamples_; i__++) {
			//chargement de la matrice path par la trajectoire.
			mod_->asset(path, t, opt_->T_, opt_->nbTimeSteps_, rng_, past);
			//pnl_mat_print(path);
			//avoir la matrice shift� pour l'actif d
			mod_->shiftAsset(shift_path, path, d, h, t, timestep);
			//pnl_mat_print(shift_path);

			mod_->shiftAsset(shift_path_inverse, path, d, -h, t, timestep);
			//pnl_mat_print(shift_path_inverse);

		   //ajout du resultat du payoff � l'esperance empirique.
			diff = opt_->payoff(shift_path) - opt_->payoff(shift_path_inverse);
			Delta += diff;
			var += diff * diff;
		}

		param_actualisation /= pnl_vect_get(V, d - 1);
		Delta *= param_actualisation;
		var *= (param_actualisation*(nbSamples_))*(param_actualisation*(nbSamples_));
		var /= nbSamples_;
		//stockage de Delta en delta � la position d
		pnl_vect_set(delta, d - 1, Delta);
		pnl_vect_set(ic, d - 1, 2 * 1.96*sqrt((var - Delta * Delta) / nbSamples_));
	}
}

PnlVect *MonteCarlo::prices_list(const PnlMat *marche,PnlVect *ic) {
	int H = marche->m;
	ic = pnl_vect_create_from_scalar(H, 0);
	PnlVect *prices_list = pnl_vect_create_from_scalar(H, 0);

	//initialisation de la matrice past par les spots.(premiere ligne de march�)
	PnlMat *past = pnl_mat_create_from_scalar(1, opt_->size_, 0);
	PnlVect *extracted_vec = pnl_vect_create(opt_->size_);
	pnl_mat_get_row(extracted_vec, marche, 0);
	pnl_mat_set_row(past, extracted_vec, 0);

	double p_ = 0;
	double ic_ = 0;
	this->price(p_, ic_);
	pnl_vect_set(prices_list, 0, p_);
	pnl_vect_set(prices_list, 0, ic_);

	int nb_rows = 1;
	int count = 1;
	for (int i = 1; i <= H; i++) {
		if (i*opt_->T_ / H == count * opt_->T_ / opt_->nbTimeSteps_) {
			count += 1;
			nb_rows += 1;
			pnl_mat_resize(past, nb_rows, mod_->size_);
		}
		pnl_mat_get_row(extracted_vec, marche, i);
		pnl_mat_set_row(past, extracted_vec, nb_rows - 1);
		this->price(past, i*opt_->T_ / H, p_, ic_);
		pnl_vect_set(prices_list, i, p_);
		pnl_vect_set(prices_list, i, ic_);
	}
	pnl_mat_free(&past);
	pnl_vect_free(&extracted_vec);
	return prices_list;
}


//fonction qui permet de calculer le prix d'une option � n'importe quelle instant t

void MonteCarlo::price(const PnlMat *past, double t, double &prix, double &ic) {
	PnlMat *path = pnl_mat_create(opt_->nbTimeSteps_ + 1, opt_->size_);
	double results = 0;
	double esp_carre = 0;
	for (int i = 0; i < nbSamples_; i++) {
		mod_->asset(path, t, opt_->T_, opt_->nbTimeSteps_, rng_, past);
		double payOff = opt_->payoff(path);
		results += payOff;
		esp_carre += payOff * payOff;
	}
	results = (results / nbSamples_);
	esp_carre = (esp_carre / nbSamples_);
	double esp = results;
	prix = exp(-mod_->r_*((opt_->T_) - t))*results;

	//calcul du ic
	double estim_carre = exp(-2 * mod_->r_*((opt_->T_) - t))*(esp_carre - esp * esp);
	ic = 2 * 1.96*sqrt(estim_carre / nbSamples_);
	pnl_mat_free(&path);

}

// si on est l'instant i*T/data->m, alors modification de matrice past tel que past->m = min{k / i*T/data->m <= k*T/past->m} - 1
void MonteCarlo::passe_adapte(int i,const PnlMat *marche,PnlMat *past) {
	PnlVect *V = pnl_vect_create(marche->n);
	pnl_mat_get_row(V, marche, i);
	int position_past = int((i*past->m )/ marche->m);
	pnl_mat_set_row(past, V, position_past);
}

void MonteCarlo::Profit_and_loss(const PnlMat* marche, PnlVect *prices_list, double &PL) {
	
	int H = marche->m - 1;
	if (prices_list->size != H) {
		pnl_vect_resize(prices_list, H);
	}

	PnlMat *past = pnl_mat_create_from_scalar(opt_->nbTimeSteps_ + 1, opt_->size_, 0);
	PnlVect *S_0 = pnl_vect_create(opt_->size_);
	pnl_mat_get_row(S_0, marche, 0);
	pnl_mat_set_row(past, S_0, 0);


	/* Calcul de P&L*/
	double p_0 = 0;
	double ic = 0;
	this->price(p_0, ic);
	PnlVect *icdelta = pnl_vect_create(mod_->size_);
	PnlVect *delta_0 = pnl_vect_create(mod_->size_);
	this->delta(past, 0.0, delta_0, icdelta);
	//PnlVect *S_0 = pnl_vect_create(mod_->size_);
	//pnl_mat_get_row(S_0, marche, 0);
	PnlVect *V = pnl_vect_create(H + 1);
	pnl_vect_set(V, 0, p_0 - pnl_vect_scalar_prod(delta_0, S_0));

	PnlVect *delta_pres = pnl_vect_copy(delta_0);
	PnlVect *delta_act = pnl_vect_create(mod_->size_);
	PnlVect *S_i = pnl_vect_create(mod_->size_);
	PnlVect *vect_diff = pnl_vect_create_from_zero(mod_->size_);
	double v_i = 0;
	/*int nb_rows = 1;
	int count = 1;*/
	for (int i = 1; i <= H; i++) {
		/*if (i*opt_->T_ / H == count * opt_->T_ / opt_->nbTimeSteps_) {
			count += 1;
			nb_rows += 1;
			pnl_mat_resize(past, nb_rows, mod_->size_);
		}
		pnl_mat_get_row(extracted_vec, marche, i);
		pnl_mat_set_row(past, extracted_vec, nb_rows - 1);*/
		passe_adapte(i, marche, past);
		if (i != H) {
			this->delta(past, i*opt_->T_ / H, delta_act, icdelta);
		}
		//� v�rifi� si t=T price donne le payoff(past);
		// ajout des prix au instant t=i*T/H � prices_list
		//this->price(past, i*opt_->T_ / H , p_0, ic);
		//pnl_vect_set(prices_list, i, p_0);

		//Update Portefeuille
		vect_diff = pnl_vect_copy(delta_act);
		pnl_vect_minus_vect(vect_diff, delta_pres);
		pnl_mat_get_row(S_i, marche, i);
		v_i = pnl_vect_get(V, i - 1)*exp(mod_->r_*opt_->T_ / H) - pnl_vect_scalar_prod(vect_diff, S_i);
		pnl_vect_set(V, i, v_i);
		delta_pres = pnl_vect_copy(delta_act);
		//
	}
	double v_h = pnl_vect_get(V, H);
	pnl_mat_get_row(S_i, marche, H);
	PL = v_h + pnl_vect_scalar_prod(delta_pres, S_i) - opt_->payoff(past);



	pnl_mat_free(&past);
	//pnl_vect_free(&extracted_vec);
	pnl_vect_free(&delta_0);
	pnl_vect_free(&S_0);
	pnl_vect_free(&V);
	pnl_vect_free(&delta_pres);
	pnl_vect_free(&S_i);
	pnl_vect_free(&vect_diff);
	pnl_vect_free(&delta_act);


}

void MonteCarlo::PL(const PnlMat* marche, PnlVect *prices_list, double &PL) {
	int H = marche->m - 1;
	cout << " marche size :" <<marche->m << endl;

	/*if (prices_list->size != H) {
		pnl_vect_resize(prices_list, H);
	}*/
	
	/* initialisation de la matrice past et spots en Assets */
	PnlMat *past = pnl_mat_create_from_double(opt_->nbTimeSteps_ + 1, opt_->size_, 0.);
	PnlVect *Assets = pnl_vect_create_from_double(mod_->size_,0.);
	pnl_mat_get_row(Assets, marche, 0);
	pnl_mat_set_row(past, Assets, 0);



	/*initialisation du portefeuille de couverture*/

	double p_0 = 0;
	double ic = 0;
	this->price(p_0, ic);
	PnlVect *icdelta = pnl_vect_create_from_double(mod_->size_, 0.);
	PnlVect *delta = pnl_vect_create_from_double(mod_->size_,0.);
	this->delta(past, 0.0, delta, icdelta);
	pnl_mat_get_row(Assets, marche, 0);
	double zero_coupon = p_0 - pnl_vect_scalar_prod(delta, Assets);
	//PL = zero_coupon;
	Portefeuille *Pf_couverture = new Portefeuille(delta,zero_coupon,Assets,mod_->r_,opt_->T_,H);
	cout << "valeur portefeuille couverture : " << Pf_couverture->Pf_Value() << endl;
	cout << " ------- carac Pf -------- : " << endl;
	cout << "Asset -------- : " << endl;
	pnl_vect_print(Pf_couverture->Assets);
	cout << "Deltas -------- : " << endl;
	pnl_vect_print(Pf_couverture->Deltas);
	cout << "Z_C -------- : " << endl;
	cout << Pf_couverture->zero_coupon<< endl;
	cout << "Interest -------- : " << endl;
	cout << Pf_couverture->Interest<< endl;
	cout << "Maturit� -------- : " << endl;
	cout << Pf_couverture->maturity << endl;
	cout << "H -------- : " << endl;
	cout << Pf_couverture->H << endl;

	cout << "fin " << endl;

	/* fin initialisation */

	/*boucle sur la grille du march�*/
	for (int i = 1; i <= H; i++) {	
		passe_adapte(i, marche, past);
		if (i != H) {
			this->delta(past, i*opt_->T_ / H, delta, icdelta);
		}

		//Update Portefeuille
		pnl_mat_get_row(Assets, marche, i);
		Pf_couverture->Update_Pf(delta, Assets);
		
		// ajout des prix au instant t=i*T/H � prices_list
		this->price(past, i*opt_->T_ / H, p_0, ic);
		std::cout << "Prix : " << p_0 - Pf_couverture->Pf_Value() << std::endl;
		//pnl_vect_set(prices_list, i, p_0);
		// � v�rifi� si t = T price donne le payoff(past);

	}
	/*for (int i = 1; i <= 20; i++) {
		passe_adapte(i, marche, past);
		if (i != H) {
			this->delta(past, i*opt_->T_ / H, delta, icdelta);
		}

		//Update Portefeuille
		pnl_mat_get_row(Assets, marche, i);
		Pf_couverture->Update_Pf(delta, Assets);
		this->price(past, i*opt_->T_ / H, p_0, ic);
		std::cout << "Prix : " << p_0- Pf_couverture->Pf_Value() << std::endl;
		// ajout des prix au instant t=i*T/H � prices_list
		//this->price(past, i*opt_->T_ / H, p_0, ic);
		//pnl_vect_set(prices_list, i, p_0);
		// � v�rifi� si t = T price donne le payoff(past);

	}*/
	double v_h = Pf_couverture->zero_coupon;
	PL = Pf_couverture->Pf_Value() - opt_->payoff(past, true);



	pnl_mat_free(&past);
	pnl_vect_free(&delta);
	pnl_vect_free(&Assets);
}