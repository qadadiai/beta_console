#pragma once
#include "pnl/pnl_random.h"
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"

/// \brief Mod�le de Black Scholes
class BlackScholesModel
{
public:
	int size_; /// nombre d'actifs du mod�le
	double r_; /// taux d'int�r�t
	double rho_; /// param�tre de corr�lation
	PnlVect *sigma_; /// vecteur de volatilit�s
	PnlVect *spot_; /// valeurs initiales des sous-jacents
	PnlVect *trend_;
	PnlMat *chol_;
	BlackScholesModel(); /// Constructeur par d�faut
	BlackScholesModel(int size, double r_, double rho_, PnlVect *sigma_, PnlVect *spot_); /// Constructeur complet
	BlackScholesModel(int size, double r_, double rho_, PnlVect *sigma_, PnlVect *spot_, PnlVect *trend_); /// Constructeur complet avec trend
	BlackScholesModel(const BlackScholesModel &BSM); /// Constructeur par recopie
	void simul_market(PnlMat *path, double T, int H, PnlRng *rng);
	~BlackScholesModel(); /// Destructeur
	BlackScholesModel& operator = (const BlackScholesModel &BSM); /// Op�rateur d'affectation =

	/**
	 * permet d'inialiser la matrice de cholesky
	 */
	void initalizeChol();
	/**
	 * G�n�re une trajectoire du mod�le et la stocke dans path
	 *
	 * @param[out] path contient une trajectoire du mod�le.
	 * C'est une matrice de taille (nbTimeSteps+1) x d
	 * @param[in] T  maturit�
	 * @param[in] nbTimeSteps nombre de dates de constatation
	 */
	void asset(PnlMat *path, double T, int nbTimeSteps, PnlRng *rng);

	/**
	 * Calcule une trajectoire du sous-jacent connaissant le
	 * pass� jusqu' � la date t
	 *
	 * @param[out] path  contient une trajectoire du sous-jacent
	 * donn�e jusqu'� l'instant t par la matrice past
	 * @param[in] t date jusqu'� laquelle on connait la trajectoire.
	 * t n'est pas forc�ment une date de discr�tisation
	 * @param[in] nbTimeSteps nombre de pas de constatation
	 * @param[in] T date jusqu'� laquelle on simule la trajectoire
	 * @param[in] past trajectoire r�alis�e jusqu'a la date t
	 */
	void asset(PnlMat *path, double t, double T, int nbTimeSteps, PnlRng *rng, const PnlMat *past);

	/**
	 * Shift d'une trajectoire du sous-jacent
	 *
	 * @param[in]  path contient en input la trajectoire
	 * du sous-jacent
	 * @param[out] shift_path contient la trajectoire path
	 * dont la composante d a �t� shift�e par (1+h)
	 * � partir de la date t.
	 * @param[in] t date � partir de laquelle on shift
	 * @param[in] h pas de diff�rences finies
	 * @param[in] d indice du sous-jacent � shifter
	 * @param[in] timestep pas de constatation du sous-jacent
	 */
	void shiftAsset(PnlMat *shift_path, const PnlMat *path, int d, double h, double t, double timestep);

};
