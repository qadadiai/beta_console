#pragma once
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"
#define DLLEXP   __declspec( dllexport )
namespace Computations {
	DLLEXP void calleuro(double &ic, double &prix, int nb_samples, double T,
		double S0, double K, double sigma, double r);

	DLLEXP void testPayoffActigo(double &prix);

	
	
	
	DLLEXP void testAsianOption(double &prix, double &ic);
	
	DLLEXP double prix_sans_pourcentage(double &res_attendu);
	DLLEXP double prix_en_pourcentage(double &res_attendu);
	DLLEXP void testPayoffActigo(double &prix, double &resultat_attendu, bool type_path);
	DLLEXP void PrixProduit(double &prix, double &ic);
	DLLEXP void PrixProduitAndDeltas(double &prix, double &ic, PnlVect *delta, PnlVect *icDelta, double t, PnlMat *past);
	DLLEXP void listPrices(PnlVect *listPrix);
	DLLEXP void profitAndLost(double &PL);
	DLLEXP void test_complexite_Deltas();
	DLLEXP void PrixAsian(double &prix, double &ic);
	DLLEXP void test_Asset();

}