#pragma once

#include "pch.h"
#include <algorithm>

/// \brief Classe Option abstraite

#include "Actigo90.hpp"

Actigo90::Actigo90() :Option() {
}

Actigo90::Actigo90(double T, int nbTimeSteps, int size) : Option(T, nbTimeSteps, size) {}

Actigo90::Actigo90(const Actigo90 &A) {
	T_ = A.T_;
	nbTimeSteps_ = A.nbTimeSteps_;
	size_ = A.size_;
	PayOff_coefficient = pnl_vect_copy(A.PayOff_coefficient);
}

Actigo90& Actigo90::operator=(const Actigo90 &A) {
	T_ = A.T_;
	nbTimeSteps_ = A.nbTimeSteps_;
	size_ = A.size_;
	PayOff_coefficient = A.PayOff_coefficient;
	return *this;
}

Actigo90::~Actigo90() {
	pnl_vect_free(&PayOff_coefficient);
}

double Actigo90::payoff(const PnlMat *path, bool type_path) {
	// type_path est vrai si path contient les valeurs des indices, false si les indices sont en pourcentage
	double result = 0.0;
	if (type_path) {
		PnlVect *VectInverseSpot = pnl_vect_create(size_);
		pnl_mat_get_row(VectInverseSpot, path, 0);
		for (int k = 0; k < size_; k++) {
			LET(VectInverseSpot, k) = 1 / GET(VectInverseSpot, k);
		}
		PnlVect *currentRow = pnl_vect_create(size_);
		for (int i = 1; i <= nbTimeSteps_; i++) {
			pnl_mat_get_row(currentRow, path, i);
			result += std::max(pnl_vect_scalar_prod(currentRow, VectInverseSpot) - 3.0, 0.0);
		}
		result = result / (size_*nbTimeSteps_);
		result += 0.9;
		pnl_vect_free(&VectInverseSpot);
		pnl_vect_free(&currentRow);
	}
	else {
		PnlVect *Vectunite = pnl_vect_create_from_scalar(size_,1);
		PnlVect *currentRow = pnl_vect_create(size_);
		for (int i = 1; i <= nbTimeSteps_; i++) {
			pnl_mat_get_row(currentRow, path, i);
			result += std::max(pnl_vect_scalar_prod(currentRow, Vectunite), 0.0);
		}
		result = result /(100*(size_*nbTimeSteps_));
		result += 0.9;
		pnl_vect_free(&Vectunite);
		pnl_vect_free(&currentRow);
	}
	return result;
}