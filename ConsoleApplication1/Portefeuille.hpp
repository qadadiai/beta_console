#pragma once
#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"
class Portefeuille{
public:
	PnlVect *Deltas;
	PnlVect *Assets;
	double zero_coupon;
	double Interest;
	double maturity;
	double H;
	Portefeuille();
	Portefeuille(PnlVect *Deltas, double zero_coupon, PnlVect *Assets,double Interest,double maturity,double H);
	Portefeuille(const Portefeuille &Pf);
	Portefeuille& operator=(const Portefeuille &Pf);
	~Portefeuille();
	double Pf_Value();
	void Update_Pf(PnlVect *Deltas,PnlVect *Assets);
};